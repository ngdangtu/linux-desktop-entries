# Postman

Official Site: https://www.postman.com

## Installation

1. Download the software: https://www.postman.com/downloads
2. Extract the compress file, we'll have a directory contains a directory child (`app/`) and a symbolic link `Postman`
3. Move the `app/` directory to `/opt/postman/`
4. Create symlink to the executable file: ` ln -s /opt/postman/postman /usr/local/bin/postman `
5. Download the desktop file (`postman.desktop`) and move it to `/usr/local/share/applications` directory.
