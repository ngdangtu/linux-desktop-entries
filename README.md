# Linux Desktop Entry Collection

https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html

<nav style='display:flex;justify-content:center'>
    <a href='#instruction'>Instruction</a>
    <hr class='border:none;bacground:transparent;width:1ch' /> 
    <a href='#walkthrough'>Contributing</a>
</nav>

<small>What is Desktop Entry, refer [this article](https://wiki.archlinux.org/title/Desktop_entries).</small>

> Desktop entries for applications, or .desktop files, are generally a combination of meta information resources and a shortcut of an application. These files usually reside in `/usr/share/applications/` or `/usr/local/share/applications/` for applications installed system-wide, or `~/.local/share/applications/` for user-specific applications. User entries take precedence over system entries. 

However, not all programs offer this file for manual installation so I create a collection to keep `.desktop` files for all those programs. 

## Instruction

Simply replace the raw link of desktop file, that you wish to install, with the Firefox link below:

```bash
wget https://raw.githubusercontent.com/mozilla/sumo-kb/main/install-firefox-linux/firefox.desktop -P /usr/local/share/applications
```

**Credit**: [Installation page of Firefox](https://support.mozilla.org/en-US/kb/install-firefox-linux)

## Repository Structure

The repository are structured as `app-name/version/distro/file.desktop`. This way, it can support desktop files for different software versions or distros. To keep thing consistency, we have a few rules for directory name:

- `app-name` » the name that you could call from command line (which is no space)
- `version` » always starts with `v` than major number and is used for all the version come later. Avoid adding other number if not necessary.
- `distro` » default is `any`, otherwise name rule is similar to _app-name_.
- `file.desktop` » this is the file name. Always replace the file basename (`file`) with _app-name_.

## Contributing

You are welcome to fire issues for request desktop files of any Linux softwares, except those already have one like Firefox. You are even more welcome to do add desktop files from PR, it could save everyone time a lots.
